using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioSource _audioSource;
    public AudioClip _clickReleaseSound, _clickSound;
    
    public void OnStartButtonClick(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void OnQuitButtonClick()
    {
        Application.Quit();
    }
    
    public void PlayClickReleaseSound()
    {
        _audioSource.PlayOneShot(_clickReleaseSound);
    }
    public void PlayClickSound()
    {
        _audioSource.PlayOneShot(_clickSound);
    }
    
}
