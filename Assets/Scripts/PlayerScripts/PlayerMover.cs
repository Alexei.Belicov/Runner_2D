﻿using UnityEngine;

namespace PlayerScripts
{
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _stepSize;
        [SerializeField] private float _maxHeight;
        [SerializeField] private float _minHeight;
        // [SerializeField] private Player _player;
        // [SerializeField] private Animator _playerAnimator;
        private Animator _animator;

        private Vector3 _targetPosition;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _targetPosition = transform.position;
        }

        private void Update()
        {
            if (transform.position != _targetPosition)
            {
                transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _moveSpeed * Time.deltaTime);
            }
        }

        public void TryMoveUp()
        {
            if (_targetPosition.y < _maxHeight)
            {
                SetNextPosition(_stepSize);
                _animator.Play("PlayerJump", 0, 0f);
            }
        }

        public void TryMoveDown()
        {
            if (_targetPosition.y > _minHeight)
            {
                SetNextPosition(-_stepSize);
                _animator.Play("PlayerJump", 0, 0f);
            }
        }

        private void SetNextPosition(float stepSize)
        {
            _targetPosition = new Vector2(_targetPosition.x, _targetPosition.y + stepSize);
        }
    }
}