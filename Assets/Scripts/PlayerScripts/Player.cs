﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

namespace PlayerScripts
{
    public class Player : MonoBehaviour
    {
        public event UnityAction<int> HealthChangedEvent;
        public event UnityAction DiedEvent;
        public event UnityAction TimerEvent;
        
        [SerializeField] private int _health;
        [SerializeField] private TMP_Text _timerText;
        [SerializeField] private float _timer;
        [SerializeField] private GameObject _hurtEffectPrefab;
        //[SerializeField] private ParticleSystem _hurtParticle;
        private Collider2D _collider2D;
        //private Animator _animator;
        private Vector3 _offset;

        
        private void Start()
        {
            //_animator = GetComponent<Animator>();
            _collider2D = GetComponent<Collider2D>();
            _offset = Vector3.up;
            HealthChangedEvent?.Invoke(_health);
        }

        private void Update()
        {
            Timer();
        }

        public void ApplyDamage(int damage)
        {
            _health -= damage;
            InstantiateHurtEffect();

            HealthChangedEvent?.Invoke(_health);

            StopCoroutine(OnHit());
            StartCoroutine(OnHit());
            
            if (_health <= 0)
                Die();
        }

        private void Die()
        {
            DiedEvent?.Invoke();
            //_animator.Play("PlayerDie", 0, 0f);
            _timerText.gameObject.SetActive(false);
        }

        private void Win()
        {
            TimerEvent?.Invoke();
            //_animator.Play("PlayerTripOver", 0, 0f);
            _timerText.gameObject.SetActive(false);
        }
  
        private void Timer()
        {
            _timer -= Time.deltaTime;
            _timerText.text = _timer.ToString("F0");
            
            if (_timer <= 0)
            {
                Win();
            }
        }
        
        private IEnumerator OnHit()
        {
            _collider2D.enabled = false;
            //Debug.Log(message: "Collider disabled");
            //_animator.Play("PlayerHurt", 0, 0f);

           yield return new WaitForSeconds(seconds: 1f);
            _collider2D.enabled = true;
            //Debug.Log(message: "Collider enabled");
        }
        
        private void InstantiateHurtEffect()
        {
            Quaternion quaternion = new Quaternion(1, 0, 0, 0);
            GameObject deathEffectClone = Instantiate(_hurtEffectPrefab, transform.position + _offset, quaternion);
            Destroy(deathEffectClone, 1f);
        }
    }
}