using UnityEngine;

namespace PlayerScripts
{
    [RequireComponent(typeof(AudioSource))]
    public class PlayerFootsteps : MonoBehaviour
    {
        [SerializeField] AudioClip[] _currentFootsteps;
        [SerializeField] private float _volumeScale = 0.3f;
        private AudioSource _audioSource;
        private int _randomAudioClip, _previousAudioClip;

        void Start()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlayFootstep()
        {
            _randomAudioClip = Random.Range(0, _currentFootsteps.Length);
            if (_randomAudioClip == _previousAudioClip) // если сгенерировалось значение, равное предыдущему
                _randomAudioClip = _randomAudioClip + 1;
            if (_randomAudioClip == _currentFootsteps.Length) // если вылезли за границы массива
                _randomAudioClip = 0;
            _previousAudioClip = _randomAudioClip;
            _audioSource.PlayOneShot(_currentFootsteps[_randomAudioClip], _volumeScale);
        }
    }
}
