﻿using UnityEngine;

public class Spawner : ObjectPool
{
    [SerializeField] private GameObject[] _enemyPrefabs;
    [SerializeField] private Transform[] _spawnPoints;
    [SerializeField] private float _secondsBetweenSpawn;

    private float _elapsedTime = 0f;
    private int _randomSpawnPointNumber = 0;
    private int _previousSpawnPointNumber = 0;

    private void Start()
    {
        Initialize(_enemyPrefabs);
    }

    private void Update()
    {
        _elapsedTime += Time.deltaTime;

        if (_elapsedTime >= _secondsBetweenSpawn)
        {
            if (TryGetObject(out GameObject enemy))
            {
                _elapsedTime = 0f;

                _randomSpawnPointNumber = Random.Range(0, _spawnPoints.Length);

                // Try not to spawn an enemy at the same point twice
                if (_randomSpawnPointNumber == _previousSpawnPointNumber)
                    _randomSpawnPointNumber = _previousSpawnPointNumber + 1;
                if (_randomSpawnPointNumber == _spawnPoints.Length)
                    _randomSpawnPointNumber = 0;
                _previousSpawnPointNumber = _randomSpawnPointNumber;

                SetEnemy(enemy, _spawnPoints[_randomSpawnPointNumber].position);
            }
        }
    }

    private void SetEnemy(GameObject enemy, Vector3 spawnPoint)
    {
        enemy.SetActive(true);
        enemy.transform.position = spawnPoint;
    }
}