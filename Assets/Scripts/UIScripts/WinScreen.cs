using UnityEngine;
using PlayerScripts;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UIScripts
{
    [RequireComponent(typeof(CanvasGroup))]
    public class WinScreen : MonoBehaviour
    {
        [SerializeField] private HealthPanel _healthPanel;
        [SerializeField] private Player _player;
        [SerializeField] private Spawner _spawner;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _nextLevelButton;
        [SerializeField] private GameOverScreen _gameOverScreen;
        
        private CanvasGroup _winGroup;

        private void OnEnable()
        {
            _player.TimerEvent += OnTimer;
            _nextLevelButton.onClick.AddListener(OnNextLevelButtonClick);
            _exitButton.onClick.AddListener(OnExitButtonClick);
        }

        private void Start()
        {
            _winGroup = GetComponent<CanvasGroup>();
            _winGroup.alpha = 0f;
            _winGroup.interactable = false;
        }
        
        private void OnDisable()
        {
            _player.TimerEvent -= OnTimer;
            _nextLevelButton.onClick.RemoveListener(OnNextLevelButtonClick);
            _exitButton.onClick.RemoveListener(OnExitButtonClick);
        }
        
        private void OnTimer()
        {
            _winGroup.alpha = 1f;
            _winGroup.interactable = true;
            Time.timeScale = 0f;
            _healthPanel.DisableHealthPanel();
            _gameOverScreen.gameObject.SetActive(false);
            _player.gameObject.SetActive(false);
            _spawner.gameObject.SetActive(false);       
        }
        
        private void OnNextLevelButtonClick()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        
        private void OnExitButtonClick()
        {
            // Works only in build
            Application.Quit();
        }
    }
}
