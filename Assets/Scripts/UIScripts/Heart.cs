using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIScripts
{
    [RequireComponent(typeof(Image))]
    public class Heart : MonoBehaviour
    {
        [SerializeField] private float _lerpDuration;
        
        private Image _image;

        private void Awake()
        {
            _image = GetComponent<Image>();
            _image.fillAmount = 1f;
        }

        public void ToFill()
        {
            StartCoroutine(Filling(0f, 1f, _lerpDuration, Fill));
        }

        public void ToEmpty()
        {
            StartCoroutine(Filling(1f, 0f, _lerpDuration, Destroy));
        }
        
        private IEnumerator Filling(float startValue, float endValue, float duration, UnityAction<float> lerpEndEvent)
        {
            float elapsedTime = 0f;

            while (elapsedTime < duration)
            {
                var nextValue = Mathf.Lerp(startValue, endValue, elapsedTime / duration);
                _image.fillAmount = nextValue;
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            lerpEndEvent?.Invoke(endValue);
        }

        private void Destroy(float value)
        {
            _image.fillAmount = value;
            Destroy(gameObject);
        }

        private void Fill(float value)
        {
            _image.fillAmount = value;
        }
    }
}
