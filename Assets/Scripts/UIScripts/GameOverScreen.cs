﻿using PlayerScripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UIScripts
{
    [RequireComponent(typeof(CanvasGroup))]
    public class GameOverScreen : MonoBehaviour
    {
        [SerializeField] private HealthPanel _healthPanel;
        [SerializeField] private Player _player;
        [SerializeField] private Spawner _spawner;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private WinScreen _winScreen;

        private CanvasGroup _gameOverGroup;

        private void OnEnable()
        {
            _player.DiedEvent += OnDied;
            _restartButton.onClick.AddListener(OnRestartButtonClick);
            _exitButton.onClick.AddListener(OnExitButtonClick);
        }

        private void Start()
        {
            _gameOverGroup = GetComponent<CanvasGroup>();
            _gameOverGroup.alpha = 0f;
            _gameOverGroup.interactable = false;
        }

        private void OnDisable()
        {
            _player.DiedEvent -= OnDied;
            _restartButton.onClick.RemoveListener(OnRestartButtonClick);
            _exitButton.onClick.RemoveListener(OnExitButtonClick);
        }

        private void OnDied()
        {
            _gameOverGroup.alpha = 1f;
            _gameOverGroup.interactable = true;
            Time.timeScale = 0f;
            _healthPanel.DisableHealthPanel();
            _winScreen.gameObject.SetActive(false);
            _player.gameObject.SetActive(false);
            _spawner.gameObject.SetActive(false);
        }

        private void OnRestartButtonClick()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private void OnExitButtonClick()
        {
            // Works only in build
            Application.Quit();
        }
    }
}