﻿using PlayerScripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UIScripts
{
    public class HealthPanel : MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private Image _healthImage;
        [SerializeField] private TMP_Text _healthText;

        private void OnEnable()
        {
            _player.HealthChangedEvent += OnHealthChanged;
        }

        private void OnDisable()
        {
            _player.HealthChangedEvent -= OnHealthChanged;
        }
        
        private void OnHealthChanged(int health)
        {
            _healthText.text = health.ToString();
        }

        public void DisableHealthPanel()
        {
            _healthText.alpha = 0f;
            _healthImage.enabled = false;
        }
    }
}
